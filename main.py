def count_wins(player_one_choices, player_two_choices):
    wins_player_one = 0
    wins_player_two = 0
    draws = 0

    for choice_one, choice_two in zip(player_one_choices, player_two_choices):
        if choice_one == choice_two:
            draws += 1
        elif (
            (choice_one == "R" and choice_two == "S")
            or (choice_one == "S" and choice_two == "P")
            or (choice_one == "P" and choice_two == "R")
        ):
            wins_player_one += 1
        else:
            wins_player_two += 1

    return wins_player_one, wins_player_two, draws

with open("player1.txt", "r") as file:
    player_one_choices = file.read().splitlines()

with open("player2.txt", "r") as file:
    player_two_choices = file.read().splitlines()

player_one_wins, player_two_wins, num_draws = count_wins(player_one_choices, player_two_choices)

with open("result.txt", "w") as file:
    file.write("Player1 wins: {}\n".format(player_one_wins))
    file.write("Player2 wins: {}\n".format(player_two_wins))
    file.write("Draws: {}\n".format(num_draws))
